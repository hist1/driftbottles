## DriftBottles

### 1. Reference

#### For ERD, please visit the link below:
https://lucid.app/lucidchart/849af719-24aa-40af-95d2-0b551250c988/edit?invitationId=inv_6bb44787-cef9-4351-b1e5-8cec4dd47ed8

#### For wireframe, please visit the link below:
https://www.figma.com/proto/UPRnP97zteMhzSv9cJwogF/Dating-App?node-id=1%3A2&scaling=scale-down&page-id=0%3A1&starting-point-node-id=1%3A2

### Frontend

#### install package for mac OS
```bash
cd frontend
yarn install
cd ios
pod install
cd ..
```

#### run frontend

```bash
cd frontend
npx react-native start
npx react-native run-ios
npx react-native run-android
```

### Backend

#### install nest in Global
```bash
npm install -g @nestjs/cli
nest --help # run this to check if nest is installed
```

#### run backend
```bash
cd backend
yarn run start
```
